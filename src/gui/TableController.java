package gui;

import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import expenses.Bill;
import expenses.Purchase;

/**
 * @author Ahmed Shaheen This class is responsible on creating the Purchase
 *         table and the Bill table as well adding records and deleting.
 */

public class TableController {

	private JTable table;

	/**
	 * Constructor for the table class.
	 * 
	 * @param table
	 */

	public TableController(JTable table) {
		this.table = table;
	}

	/**
	 * Select a row from the table
	 * 
	 * @param the
	 *            selected row
	 */
	public void SelectRow(int row) {
		table.changeSelection(row, 1, false, false);
	}

	/**
	 * The method for adding a record to the Purchase table.
	 * 
	 * @param list
	 *            is the array that needs to be added to the table
	 */
	public void addToPurchaseTable(ArrayList<Purchase> list) {
		DefaultTableModel y = (DefaultTableModel) table.getModel();

		for (int i = 0; i < list.size(); i++) {
			Vector rowData = new Vector();
			Purchase tmp = list.get(i);

			rowData.add(tmp.getDescription());
			rowData.add(tmp.getAmount());
			rowData.add(tmp.getDate());
			rowData.add(tmp.isPaid());
			rowData.add(tmp.getDueDate());

			y.addRow(rowData);

		}

	}

	/**
	 * The method for adding a record to the Bill table.
	 * 
	 * @param list
	 *            is the array that needs to be added to the table
	 */
	public void addToBillTable(ArrayList<Bill> list) {
		DefaultTableModel y = (DefaultTableModel) table.getModel();
		if (list.size() == 0)
			return;

		for (int i = 0; i < list.size(); i++) {
			Vector rowData = new Vector();
			Bill tmp = list.get(i);

			rowData.add(tmp.getDescription());
			rowData.add(tmp.isPaid());
			rowData.add(tmp.getRepetitionInterval());
			rowData.add(tmp.getAmount());
			rowData.add(tmp.getDate());

			y.addRow(rowData);

		}

	}

	/**
	 * Sets up the table format of the Purchase records
	 */
	public void setUpPurchaseTable() {
		table.setModel(new DefaultTableModel(new Object[][] {}, new String[] {
				"RetailorName", "Location", "Amount", "Date", "Status",
				"DueDate" }) {
			@Override
			public boolean isCellEditable(int row, int column) {
				// all cells false
				return false;
			}

			Class[] columnTypes = new Class[] { String.class, String.class,
					Double.class, Date.class, String.class, Date.class };

			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});
	}

	/**
	 * Sets up the table format of the Bill records
	 */
	public void setUpBillTalbe() {
		table.setModel(new DefaultTableModel(new Object[][] {}, new String[] {
				"Company Name", "Status", "Repetition Interval", "Amount", "Due date" }) {
			@Override
			public boolean isCellEditable(int row, int column) {
				// all cells false
				return false;
			}

			Class[] columnTypes = new Class[] { String.class, String.class,
					String.class, Double.class, Date.class };

			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});

	}

	/**
	 * Method for cleaning the table
	 */
	void cleanTheTable() {
		DefaultTableModel dm = (DefaultTableModel) table.getModel();

		dm.getDataVector().removeAllElements();
		dm.fireTableDataChanged();
	}

}
