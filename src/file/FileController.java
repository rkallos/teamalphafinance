package file;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * 
 * @author Ahmed Shaheen 
 * This class is responsible for creating, writing, reading, deleting a record from file
 */

public class FileController {

	File file;

	public File getFile() {
		return file;
	}

	/**
	 * Creation of the file, with the check if the file already exists 
	 * @param choice
	 */
	public void createFile(int choice) {
		if (choice == 0)
			file = new File("Purchase.txt");
		else
			file = new File("Bill.txt");
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Method that writes lines to the file with PrinWriter
	 * 
	 * @param contentline for writing
	 * @param file file to write
	 * @throws IOException
	 */
	public void writeToFile(String lineContent, File file) throws IOException {
		try (PrintWriter out = new PrintWriter(new BufferedWriter(
				new FileWriter(file.getAbsoluteFile(), true)))) {
			out.println(lineContent);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Method that reads file contents 
	 * @return array of string from the file
	 */
	public ArrayList<String> readFromFile() {
		ArrayList<String> lines = new ArrayList<String>();
		try (BufferedReader read = new BufferedReader(new FileReader(
				file.getAbsoluteFile()))) {
			String lineContent;
			while ((lineContent = read.readLine()) != null) {
				if (!lineContent.equals(""))
					lines.add(lineContent);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		return lines;
	}

	/**
	 * Method for deleting a record from the file
	 * @param string to remove from the file
	 * @throws IOException
	 */
	public void deleteRecord(String string) throws IOException {
		File temp = new File("tmp.txt");
		temp.createNewFile();
		ArrayList<String> lines = new ArrayList<String>();
		lines = readFromFile();
		for (int i = 0; i < lines.size(); i++) {
			if (!lines.get(i).equals(string)) {
				writeToFile(lines.get(i), temp);
			}
		}
		file.delete();
		temp.renameTo(file);

	}
}
