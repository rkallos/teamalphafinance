package expenses;



/**
 * @author Ahmed Shaheen
 *class to store each record of Purchase type, it has 4 fields 
 *retailorName 
 *location
 *amount
 *date
 *status
 *dueDate
 */

public class Purchase extends Expense {
	
	long dueDate;
	
	/**
	 * constructor that contains the fields values
	 * @param retailorName
	 * @param location
	 * @param amount
	 * @param date
	 * @param status
	 * @param dueDate
	 */
	
	public Purchase(String description, double amount, long date, boolean paid, long dueDate){
		super();
		this.description = description;
		this.amount = amount;
		this.date = date;
		this.paid = paid;
		this.dueDate = dueDate;
	}

	/**
	 * @return the dueDate
	 */
	public long getDueDate() {
		return dueDate;
	}

	/**
	 * @param dueDate the dueDate to set
	 */
	public void setDueDate(long dueDate) {
		this.dueDate = dueDate;
	}

	public String toString() {
		return super.toString() + ":" + this.getDueDate();
	}
}
