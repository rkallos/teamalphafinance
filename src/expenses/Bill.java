package expenses;

/**
 * @author Ahmed Shaheen
 *class to store each record of Bill type
 *it has 4 fields to store the names are speaking for themselves.
 *companyName 
 *Status
 *repetitionInterval
 *amount
 *dueDate
 */

public class Bill extends Expense {
	
	String repetitionInterval;

	/**
	 * constructor that contains the fields values
	 * @param description
	 * @param paid
	 * @param repetitionInterval
	 * @param amount
	 * @param dueDate
	 */
	
	public Bill(String description, boolean paid, String repetitionInterval, double amount, long dueDate){
		super();
		this.description = description;
		this.paid = paid;
		this.repetitionInterval = repetitionInterval;
		this.amount = amount;
		this.date = dueDate;
	}

	/**
	 * @return the repetitionInterval
	 */
	public String getRepetitionInterval() {
		return repetitionInterval;
	}

	/**
	 * @param repetitionInterval the repetitionInterval to set
	 */
	public void setRepetitionInterval(String repetitionInterval) {
		this.repetitionInterval = repetitionInterval;
	}
	
	public String toString() {
		return super.toString() + ":" + getRepetitionInterval();
	}
}
