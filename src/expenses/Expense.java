package expenses;

public abstract class Expense {
	String description;
	double amount;
	long date;
	boolean paid;
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public long getDate() {
		return date;
	}
	public void setDate(long date) {
		this.date = date;
	}
	public boolean isPaid() {
		return paid;
	}
	public void setPaid(boolean paid) {
		this.paid = paid;
	}
	
	public String toString() {
		String paid = this.isPaid() ? "Paid" : "Unpaid";
		String out = this.getDescription() + ":"
				+ this.getAmount() + ":"
				+ this.getDate() + ":"
				+ paid;
		return out;
	}
}
