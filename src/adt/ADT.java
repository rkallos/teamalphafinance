package adt;

import java.io.IOException;
import java.util.ArrayList;

import stringToClass.StringToClass;
import expenses.*;
import file.FileController;

/**
 * @author Ahmed Shaheen This class is responsible to controlling the operations
 *         of the files
 */

public class ADT {

	ArrayList<Purchase> purchaseArray;
	ArrayList<Bill> billArray;
	FileController File;
	int type; // 0 is for a Purchase and 1 is for a Bill

	/**
	 * Constructor for the ADT class, it depends on the choice.
	 * 
	 * @param choice
	 *            the value that indicates which type of file will be created if
	 *            choice is 0: Purchase file 1:Bill file
	 */

	public ADT(int choice) {
		File = new FileController();
		if (choice == 0) {

			this.type = 0;
			File.createFile(0);
			purchaseArray = StringToClass.getArrayPurchase(File.readFromFile());
		} else {
			this.type = 1;
			File.createFile(1);
			billArray = StringToClass.getArrayBill(File.readFromFile());
		}
	}

	/**
	 * Add record to the Purchase's file and array.
	 * 
	 * @param rec
	 *            record to add
	 * @throws IOException
	 */
	public void addPurchaseRecord(Purchase rec) throws IOException {
		purchaseArray.add(rec);
		File.writeToFile(rec.toString(),
				File.getFile());
	}

	/**
	 * Add record to the Bill's file and array.
	 * 
	 * @param rec
	 *            record to add
	 * @throws IOException
	 */
	public void addBillRecord(Bill rec) throws IOException {
		billArray.add(rec);
		File.writeToFile(rec.toString(), File.getFile());
	}

	/**
	 * Deletes record from the file and from the array
	 * 
	 * @param pos
	 *            position where the record will be deleted
	 * @throws IOException
	 */
	public void deleteRecord(int index) throws IOException {
		if (type == 0) {
			File.deleteRecord(purchaseArray.get(index).toString());
			purchaseArray.remove(index);
		}

		if (type == 1) {
			File.deleteRecord(billArray.get(index).toString());
			billArray.remove(index);
		}

	}

	/**
	 * @return the purchaseArray
	 */
	public ArrayList<Purchase> getPurchaseArray() {
		return purchaseArray;
	}

	/**
	 * @return the billArray
	 */
	public ArrayList<Bill> getBillArray() {
		return billArray;
	}

	/**
	 * @return the type
	 */
	public int getType() {
		return type;
	}

	/**
	 * @param purchaseArray
	 *            the purchaseArray to set
	 */
	public void setPurchaseArray(ArrayList<Purchase> purchaseArray) {
		this.purchaseArray = purchaseArray;
	}

	/**
	 * @param billArray
	 *            the billArray to set
	 */
	public void setBillArray(ArrayList<Bill> billArray) {
		this.billArray = billArray;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(int type) {
		this.type = type;
	}
}
