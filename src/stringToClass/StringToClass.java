package stringToClass;

import java.util.ArrayList;
import java.util.Date;
import org.apache.commons.lang3.StringUtils;
import expenses.Bill;
import expenses.Purchase;

/**@author Ahmed Shaheen
*This class is responsible for translating strings from the file into class format and vice versa.
*The data in file is separated by ":".
*/
public class StringToClass {
//	
//	/**
//	 * This method is responsible for translating class fields into a format for writing into the file.
//	 * @param rec the instance that we want to transform to string for writing into the file.
//	 * @return string which contains fields of Purchase class, separated by":"
//	 */
//
//	public static String ParseForWritingPurchase(Purchase rec) {
//		String string = rec.getRetailorName() + ":" + rec.getLocation() + ":"
//				+ rec.getAmount() + ":" + rec.getDate() + ":" + rec.getStatus()
//				+ ":" + rec.getDueDate();
//		return string;
//	}
//	/**
//	 * This method is responsible for translating class fields into a format for writing into the file.
//	 * @param rec the instance that we want to transform to string for writing into the file.
//	 * @return string which contains fields of Bill class, separated by":"
//	 */
//	public static String ParseForWritingBill(Bill rec) {
//		String string = rec.getDescription() + ":" + rec.isPaid() + ":"
//				+ rec.getRepetitionInterval()+ ":" + rec.getAmount() + ":" + rec.getDate();
//		return string;
//
//	}
	/**
	 * This method transforms an array of string, which was read from the Purchase file into the Array of Purchase class instances
	 * @param data array of string to translate to Purchase class instances
	 * @return return an array with Purchase class objects
	 */
	public static ArrayList<Purchase> getArrayPurchase(ArrayList<String> data) {
		ArrayList<Purchase> recordList = new ArrayList<Purchase>();
		for (int i = 0; i < data.size(); i++) {
			if (!data.get(i).equals(""))
				recordList.add(ParsePurchaseFromTable(data.get(i)));
		}
		return recordList;
	}
	/**
	 * This method transforms an array of string, which was read from the Bill file into the Array of Bill class instances
	 * @param data array of string to translate to Bill class instances
	 * @return return an array with Bill class objects
	 */
	public static ArrayList<Bill> getArrayBill(ArrayList<String> data) {
		ArrayList<Bill> recordList = new ArrayList<Bill>();
		for (int i = 0; i < data.size(); i++) {
			if (!data.get(i).equals(""))
				recordList.add(ParseBillFromTable(data.get(i)));
		}
		return recordList;
	}
	
	/**
	 * This method parses strings into Purchase class objects. Fields are separated by ":"
	 * @param S String for parsing
	 * @return object of Purchase class
	 */
	public static Purchase ParsePurchaseFromTable(String S) {
		String[] ar = StringUtils.split(S, ':');

		if (ar.length < 6) {
			System.exit(0);
		}
		if (ar.length > 6) {
			System.exit(0);
		}

		Purchase rec = new Purchase(ar[0], Double.parseDouble(ar[1]), Long.parseLong(ar[3]), Boolean.parseBoolean(ar[4]), Date.parse(ar[5]));
		return rec;
	}
	/**
	 * This method parses strings into Bill class objects. Fields are separated by ":"
	 * @param S String for parsing
	 * @return object of Bill class
	 */
	public static Bill ParseBillFromTable(String S) {
		String[] ar = StringUtils.split(S, ':');
		if (ar.length < 5) {
			System.exit(0);
		}
		if (ar.length > 5) {
			System.exit(0);
		}
		Bill rec = new Bill(ar[0], Boolean.parseBoolean(ar[1]), ar[3], Double.parseDouble(ar[3]), Date.parse(ar[4]));
		return rec;
	}
}
